# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table cancion (
  id                            serial not null,
  user_id                       integer,
  nombre                        varchar(255) not null,
  descripcion                   varchar(255),
  url_cancion                   varchar(255) not null,
  constraint pk_cancion primary key (id)
);

create table usuario (
  id                            serial not null,
  email                         varchar(255) not null,
  paswd                         varchar(255) not null,
  nombre                        varchar(255) not null,
  is_artist                     boolean default false not null,
  constraint pk_usuario primary key (id)
);

alter table cancion add constraint fk_cancion_user_id foreign key (user_id) references usuario (id) on delete restrict on update restrict;
create index ix_cancion_user_id on cancion (user_id);


# --- !Downs

alter table if exists cancion drop constraint if exists fk_cancion_user_id;
drop index if exists ix_cancion_user_id;

drop table if exists cancion cascade;

drop table if exists usuario cascade;

