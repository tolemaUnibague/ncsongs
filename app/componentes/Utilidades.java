package componentes;

import com.fasterxml.jackson.databind.node.ObjectNode;
import play.Logger;
import play.libs.Json;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Utilidades {

    /**
     * Muestra mensaje en consola
     * @param mensaje
     * @param stacktrace
     * @param contenido
     */
    public static void log(String mensaje, StackTraceElement stacktrace, String contenido) {
        Logger.debug("================ INICIO LOG ================");
        Logger.debug("== Fecha y hora: " + new SimpleDateFormat("yyyyMMdd HHmmss").format(Calendar.getInstance().getTime()));
        Logger.debug("== Mensaje: " + mensaje);
        Logger.debug("== Stacktrace: " + stacktrace.toString());
        Logger.debug("== Contenido: " + contenido);
        Logger.debug("================ Fin LOG ================");
    }

    /**
     * Estructura estandar JSON para respuesta en una petición
     * @param error para indicar si hubo un error o no (errores controlados)
     * @param contenido contenido de la respuesta
     * @return
     */
    public static ObjectNode respuesta(Boolean error, ObjectNode contenido) {
        ObjectNode jsonRespuesta = Json.newObject();
        jsonRespuesta.put("error", error);
        jsonRespuesta.set("contenido", contenido);
        return jsonRespuesta;
    }

}
