package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import componentes.Utilidades;
import models.Usuario;
import play.Logger;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class UsuariosController extends Controller {

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    public Result index() {
        return ok(views.html.index.render());
    }

    public Result crear(){

        JsonNode json = request().body().asJson();
        if(json == null) {
            return badRequest("Expecting Json data");
        } else {

            Usuario usuario = Json.fromJson(json, Usuario.class);
            Logger.debug("===========> Json: " + json);
            usuario.save();
            return ok("OK");

        }
    }
    /**
     * Acción que renderiza los datos de un usuario
     * @param id
     * @return ok
     */
    public Result obtener(Long id) {
        ObjectNode usuario = this.getDatosUsuario(id, true);
        if(usuario == null) {
            ObjectNode data = Json.newObject();
            data.put("mensaje", "No se encontró el usuario especificado");
            return notFound(Utilidades.respuesta(true, data));
        }
        return ok(Utilidades.respuesta(true, usuario));
    }

    public Result mostrar(Long id) {
        return ok(views.html.usuarios.view.render(id,"Ver usuario"));
    }

    /**
     * Busca usuario por el ID especificado, si este es encontrado, retorna un ObjectNode con
     * los atributos id, correo_electronico, nombre, esArtista.
     * En caso de que el usuario no sea encontrado, retorna NULL
     * @param id Identificador del usuario
     * @param debeSerArtista especifica si se requiere que el usuario sea artista o no
     * @return ObjectNode
     */
    private ObjectNode getDatosUsuario(Long id, Boolean debeSerArtista) {
        Usuario usuario = Usuario.getFind().byId(id);
        if(usuario == null || debeSerArtista != usuario.isArtist) return null;

        ObjectNode respuesta = Json.newObject();
        respuesta.put("id", usuario.id);
        respuesta.put("correo_electronico", usuario.email);
        respuesta.put("nombre", usuario.nombre);
        respuesta.put("esArtista", usuario.isArtist);

        return respuesta;
    }

    public Result formCrear(){
        return ok(views.html.usuarios.crearusuario.render());
    }
}
