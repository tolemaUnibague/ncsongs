package controllers;
import models.Cancion;
import models.Usuario;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Result;
import play.Logger;
import static play.mvc.Controller.request;
import static play.mvc.Http.Status.OK;
import static play.mvc.Results.badRequest;
import static play.mvc.Results.status;

import com.fasterxml.jackson.databind.JsonNode;
import sun.rmi.runtime.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by marlonramirez on 24/07/17.
 */
public class CancionController
{
    @BodyParser.Of(BodyParser.Json.class)
    public Result crear()
    {
        JsonNode json = null;
        try
        {
            json = request().body().asJson();

            if(json == null)
            {
                return badRequest("Error en formato.");
            }
            else
            {
                Cancion cancionExistente = Cancion.find.byId(json.findPath("id").asLong());

                if(cancionExistente == null)
                {
                    String nombre = json.findPath("nombre").asText();
                    nombre.trim();
                    if (!nombre.isEmpty())
                    {
                        String descripcion = json.findPath("descripcion").asText();
                        descripcion.trim();
                        if (!descripcion.isEmpty())
                        {
                            String urlCancion = json.findPath("urlCancion").asText();
                            urlCancion.trim();
                            if (!urlCancion.isEmpty())
                            {
                                JsonNode jsonNode = json.findPath("user");
                                Usuario usuario = Json.fromJson(jsonNode, Usuario.class);
                                Cancion nuevaCancion = new Cancion(usuario, nombre, descripcion, urlCancion);
                                nuevaCancion.save();
                                return status(OK);
                            }
                            else
                            {
                                return badRequest("Url de la Canción no válida.");
                            }
                        }
                        else
                        {
                            return badRequest("Descripción no válida.");
                        }
                    }
                    else
                    {
                        return badRequest("Nombre no válido.");
                    }
                }
                else
                {
                    return badRequest("Canción ya existente.");
                }
            }
        }
        catch (Exception e)
        {
            Logger.error("=================== INICIO ERROR ===================");
            Logger.error("== Date and time: " + new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime()));
            Logger.error("== Error: " + e.getMessage());
            Logger.error("== Stacktrace: ");
            e.getCause().printStackTrace();
            Logger.error("== Current Json: " + json);
            Logger.error("==================== FIN ERROR ====================");
            return badRequest("Se generó un error en el sistema, revise los logs para mayor información");
        }
    }
}
