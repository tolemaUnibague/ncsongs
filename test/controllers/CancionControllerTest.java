package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Cancion;
import models.Usuario;
import org.junit.Test;
import play.Application;
import play.inject.guice.GuiceApplicationBuilder;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import play.test.WithApplication;

import static org.junit.Assert.assertEquals;
import static play.mvc.Http.Status.BAD_REQUEST;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.POST;
import static play.test.Helpers.route;

/**
 * Created by marlonramirez on 21/07/17.
 */
public class CancionControllerTest extends WithApplication {

    @Override
    protected Application provideApplication() {
        return new GuiceApplicationBuilder().build();
    }

    @Test
    public void testCrearCancionOK()
    {
        Usuario usuario = new Usuario("test@test.com", "password", "Test", true);
        usuario.save();
        Cancion cancion = new Cancion(usuario, "Song 1", "This is a test Song", "http:www.testsong.com");

        JsonNode cancionJSON = Json.toJson(cancion);

        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(POST)
                .bodyJson(cancionJSON)
                .uri("/api/cancion/crear");

        Result result = route(app, request);
        assertEquals("Se debió agregar la canción", OK, result.status());
    }


    @Test
    public void testCrearCancionNO()
    {
        Usuario usuario = new Usuario("testError@test.com", "password", "TestError", true);
        Cancion cancion = new Cancion(usuario, "Song TEST", "This is a test Song", "http:www.testsong.com");
        JsonNode cancionJSON = Json.toJson(cancion);

        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(POST)
                .bodyJson(cancionJSON)
                .uri("/api/cancion/crear");

        Result result = route(app, request);
        assertEquals("No debería agregar una canción si el artista no existe", BAD_REQUEST, result.status());
    }
}