package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Usuario;
import org.junit.Test;
import play.Application;
import play.Logger;
import play.api.libs.json.JsValue;
import play.inject.guice.GuiceApplicationBuilder;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.test.WithApplication;

import static org.junit.Assert.assertEquals;
import static play.mvc.Http.Status.NOT_FOUND;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.GET;
import static play.test.Helpers.POST;
import static play.test.Helpers.route;

public class UsuariosControllerTest extends WithApplication {

    @Override
    protected Application provideApplication() {
        return new GuiceApplicationBuilder().build();
    }

    @Test
    public void testIndex() {

    }

    @Test
    public void testCrearUsuario() {

        Usuario testUser = new Usuario("luis.gaviria@unibague.edu.co", "1234", "Luis Gaviria", false);
        JsonNode userJson = Json.toJson(testUser);

        //This is a change, just for fourth test
        Http.RequestBuilder request = new Http.RequestBuilder()
                .bodyJson(userJson)
                .method(POST)
                .uri("/api/usuario/crear");

        Result result = route(app, request);
        assertEquals(OK, result.status());
    }
    
    @Test
    public void testVer() {
        Usuario usuario = new Usuario("test.user@email.com", "123456", "Test User", true);
        usuario.save();
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(GET)
                .uri("/api/usuario/ver/" + usuario.id);

        Result result = route(app, request);
        assertEquals(OK, result.status());
    }

    @Test
    public void testVerNotFound() {
        int id = 100;
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(GET)
                .uri("/api/usuario/ver" + id);
        Result result = route(app, request);
        assertEquals(NOT_FOUND, result.status());
    }

}
