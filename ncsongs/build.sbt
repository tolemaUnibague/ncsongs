name := """NCSongs"""
organization := "co.edu.unibague"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayEbean)

scalaVersion := "2.12.2"

libraryDependencies += guice
libraryDependencies += javaJpa
libraryDependencies += evolutions
libraryDependencies += "org.postgresql" % "postgresql" % "9.4-1201-jdbc41"
