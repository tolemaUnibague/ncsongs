
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/marlonramirez/Documentos/Development_Test/ncsongs/ncsongs/conf/routes
// @DATE:Fri Jul 21 11:12:23 COT 2017


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
