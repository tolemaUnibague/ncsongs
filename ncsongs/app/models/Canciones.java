package models;


import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.*;

/**
 * @author Luis Gaviria
 * Clase para mapear la entidad Canciones
 */
@Entity
@Table(name="Canciones")
public class Canciones extends Model {

    /**
     * Identificador de la entidad
     */
    @Id
    @GeneratedValue
    public int id;

    /**
     * Usuario al que pertenecen las canciones
     */
    @ManyToOne
    public Usuario user;

    /**
     * Nombre del usuario
     */
    public String nombre;

    /**
     * Descripción general de la canción
     */
    public String descripcion;

    /**
     * URL para cargar la canción
     */
    public String urlCancion;

    /**
     * Atributo para obtener una canción específica
     */
    public static Finder<Long, Canciones> find = new Finder<Long, Canciones>(Canciones.class);

    /**
     * Constructor de la clase
     * @param user Usuario de las canciones
     * @param nombre nombre de la cancion
     * @param descripcion descripción general de la canción
     * @param urlCancion URL de la canción para cargar
     */
    public Canciones(Usuario user, String nombre, String descripcion, String urlCancion) {
        this.user = user;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.urlCancion = urlCancion;
    }

    /**
     * Método para obtener el identificador de una canción
     * @return El identificador de la cación
     */
    public int getId() {
        return id;
    }

    /**
     * Método para setear el valor del identificador de una canción
     * @param id - Identificador de la canción
     */
    public void setId(int id) {
        this.id = id;
    }

}
