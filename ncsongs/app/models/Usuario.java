package models;


import io.ebean.Finder;
import io.ebean.Model;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Luis Gaviria
 * Clase de representa al Usuario como entidad
 */
@Entity
@Table(name="Usuario")
public class Usuario extends Model {

	/**
	 * Identificador de la entidad
	 */
    @Id
    @GeneratedValue
    public int id;

	/**
	 * Correo electrónico del usuario utilizado para el login
	 */
	@Constraints.Required
    public String email;

	/**
	 * Contraseña del usuario
	 */
	@Constraints.Required
	public String paswd;

	/**
	 * Nombre del artista y/o usuario
	 */
	@Constraints.Required
	public String nombre;

	/**
	 * Define si el usuario es artista o no
	 */
	public boolean isArtist;
	/**
	 * Atributo para obtener un Usuario específico
	 */
    public static Finder<Long, Usuario> find = new Finder<Long, Usuario>(Usuario.class);

	/**
	 * Canciones asociadas al artista
	 */
	@OneToMany(mappedBy = "user", cascade=CascadeType.ALL)
    public List<Canciones> canciones = new ArrayList<Canciones>();

	/**
	 * Método para obtener un Usuario
	 * @return Usuario como objeto
	 */
	public static Finder<Long, Usuario> getFind() {
		return find;
	}

	/**
	 * Método para obtener las canciones de un usuario
	 * @return
	 */
	public List<Canciones> getCanciones() {
		return canciones;
	}

	/**
	 * Metodo para agregar canciones a un usuario
	 * @param canciones - Lista de canciones que se quieran agregar a un Usuario
	 */
	public void setCanciones(List<Canciones> canciones) {
		this.canciones = canciones;
	}

	/**
	 * Contructor de la clase Usuario
	 * @param email Correo electrónico
	 * @param paswd Contraseña del usuario
	 * @param nombre Nombre del Usuario
	 * @param isArtist True si es artista, False si no es artista
	 */
	public Usuario(String email, String paswd, String nombre, boolean isArtist) {
		super();
		this.email = email;
		this.paswd = paswd;
		this.nombre = nombre;
		this.isArtist = isArtist;
	}

	/**
	 * Retorna el string representativo de los valores true y false
	 * @return String(SI ó NO)
	 */
	public String isArtistSt(){
		if (isArtist == true){
			return "SI";
		}else{
			return "NO";
		}
	}

	/**
	 * Método para obtener el identificador de un Usuario
	 * @return Identificador del Usuario
	 */
	public int getId() {
		return id;
	}

	/**
	 * Método para saber si un usuario es Artista
	 * @return boolean, true, si es Artista. False, si no es artista.
	 */
	public boolean isArtist() {
		return isArtist;
	}

	/**
	 * Método para definir a un Usuario como Artista
	 * @param isArtist - boolean, true, si es Artista. false, para indicar que no es Artista
	 */
	public void setArtist(boolean isArtist) {
		this.isArtist = isArtist;
	}
    
    
}
